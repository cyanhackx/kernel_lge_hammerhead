#!/bin/bash
#
#  cyanhackx
#

clear

echo ""
echo ""
echo "Start kernel build"
echo ""
echo ""

make clean
export ARCH=arm
export CROSS_COMPILE=~/tmp/arm-eabi-4.9.1/bin/arm-eabi-
export ENABLE_GRAPHITE=true
make cyanogenmod_hammerhead_defconfig
time make -j4 2>&1 | tee kernel.log

echo ""
echo "Building boot.img"
cp arch/arm/boot/zImage-dtb ../cm_ramdisk/

cd ../cm_ramdisk/

echo ""
echo "building ramdisk"
./mkbootfs boot.img-ramdisk | gzip > ramdisk.gz
echo ""
echo "making boot image"
./mkbootimg --kernel zImage-dtb --cmdline 'console=ttyHSL0,115200,n8 androidboot.hardware=hammerhead user_debug=31 msm_watchdog_v2.enable=1' --base 0x00000000 --pagesize 2048 --ramdisk_offset 0x02900000 --tags_offset 0x02700000 --ramdisk ramdisk.gz --output ../hammerhead/boot.img

rm -rf ramdisk.gz
rm -rf zImage

cd ../hammerhead/

zipfile="cyanhackx.zip"
echo ""
echo "zipping kernel"
cp boot.img zip/

rm -rf ../cm_ramdisk/boot.img

cd zip/
rm -f *.zip
zip -r -9 $zipfile *
rm -f /tmp/*.zip
cp *.zip /tmp

echo ""
echo ""
echo "Kernel build done"
echo ""
echo ""
